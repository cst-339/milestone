package com.gcu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;

import com.gcu.Database.UserDataService;
import com.gcu.Models.UserModel;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class})
public class Milestone2Application {
	@Autowired
	public static UserDataService uDS;
	
	@GetMapping("/")
	public String viewHomePage() {
		return "index";
	}
	public static void main(String[] args) {
		SpringApplication.run(Milestone2Application.class, args);
		
	
	}

}
