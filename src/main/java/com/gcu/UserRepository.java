package com.gcu;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.gcu.UserEntity;
import com.gcu.Models.UserModel;

/**
 * Repository object in charge of connecting the user model to the user database
 * @author joshbeck
 *
 */
public interface UserRepository extends MongoRepository<UserModel, String> {
	UserModel findByUsername(String username);
}
