package com.gcu.Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gcu.Database.UserDataService;
import com.gcu.Models.UserModel;
import com.gcu.Services.UserService;

/**
 * This class is the controller class for all logging in/logging out activities
 * It's main job is to route the appropriate URLs to and from the correct Thymeleaf pages
 * as well as appropriately communicate with the Service and Model classes to retrieve and store data
 * @author joshbeck
 *
 */
@Controller
public class LoginController {
	
	//Inject a singleton instance of the user service
	@Autowired
	UserService userService;
	
	@Autowired
	UserDataService uDS;
	
	//The default URL under the /login sub URL
	@GetMapping("/login")
	//Handle presenting the login form by populating it with an empty user object
	public String login(Model model) {
		return "login";
	}
}
