package com.gcu.Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gcu.Database.UserDataService;
import com.gcu.Models.UserModel;
import com.gcu.Services.UserService;

/**
 * This class is the controller class for all logging in/logging out activities
 * It's main job is to route the appropriate URLs to and from the correct Thymeleaf pages
 * as well as appropriately communicate with the Service and Model classes to retrieve and store data
 * @author joshbeck
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
	
	//Inject a singleton instance of the user service
	@Autowired
	UserDataService userDataService;

	
	//The default URL under the /login sub URL
	@GetMapping("")
	//Handle presenting the login form by populating it with an empty user object
	public String list(Model model) {
		model.addAttribute("users", userDataService.findAll());
		return "all_users";
	}
	
	//Handle the login form submission.  Be sure to encrypt the password with a hash to compare to the hashed passwords in the database
	@GetMapping("/delete")
	public String delete(@RequestParam(value="id",required=true) String id, HttpServletResponse httpServlet, Model model) throws IOException {
		model.addAttribute("id",id);
		userDataService.deleteById(id);
		return "delete_user";
	}
	
	//Logout the user and redirect the user to the login page
	@GetMapping(value="/update")
	public String update(@RequestParam(value="id",required=true) String id, HttpServletResponse httpServlet, Model model) {
		//Access the User Service to logout the user
		model.addAttribute("user", userDataService.findById(id));
		//Redirect the user to the login page
		return "update_user";
	}
}
