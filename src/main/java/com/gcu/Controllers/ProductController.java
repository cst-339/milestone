package com.gcu.Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gcu.Models.SongModel;
import com.gcu.Models.UserModel;
import com.gcu.Services.SongService;
import com.gcu.Services.UserService;

/**
 * This class is in charge of handling iterations with the product
 * @author joshbeck
 *
 */
@Controller
@RequestMapping("/songs")
public class ProductController {
	
	//Inject a singleton instance of the song service
	@Autowired
	SongService songService;
		

	/**
	 * Handle displaying the products
	 * @return the name of the product display page
	 */
	@GetMapping("")
	public String displaySongs(Model model) {
		model.addAttribute("songs", songService.getAll());
		return "all_songs";
	}
	
	@GetMapping("/add")
	public String displayNewSong(Model model) {
		model.addAttribute("songModel", new SongModel());
		return "add_new_song";
	}
	
	@PostMapping("/add/handler")
	public String handleRegistration(SongModel songModel, BindingResult bindingResult, Model model) {
		//@TODO: Registration handler simulation
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("loginMsg", "A few errors with your entry");
			model.addAttribute("songModel", songModel);
			return "add_new_song";
		}
		
		
		if (songService.add(songModel)) {
			return "song_add_success";
		} else {
			return "song_add_failure";
		}
	}
	//Handle the login form submission.  Be sure to encrypt the password with a hash to compare to the hashed passwords in the database
	@GetMapping("/delete")
	public String delete(@RequestParam("id") String id, HttpServletResponse httpServlet, Model model) throws IOException {
		model.addAttribute("id", id);
		songService.deleteById(id);
		return "delete_song";
	}
	
	//Logout the user and redirect the user to the login page
	@GetMapping(value="/update")
	public String update(@RequestParam(value="redoneTitle",required=true) String title, HttpServletResponse httpServlet, Model model) {
		//Access the User Service to logout the user
		model.addAttribute("product", songService.findByRedoneTitle(title));
		//Redirect the user to the login page
		return "update_song";
	}
}
