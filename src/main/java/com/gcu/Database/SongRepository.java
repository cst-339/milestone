package com.gcu.Database;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.gcu.Models.SongModel;
import com.gcu.Models.UserModel;

/**
 * A repository to access the database to run CRUD access for the song database
 * @author joshbeck
 *
 */
public interface SongRepository extends MongoRepository<SongModel, String> {

}
