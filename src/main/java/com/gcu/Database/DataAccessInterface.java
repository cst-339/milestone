package com.gcu.Database;

import java.util.List;

import com.gcu.UserEntity;
import com.gcu.Models.SongModel;
import com.gcu.Models.UserModel;

/**
 * Interface for handling accessing data
 * @author joshbeck
 *
 * @param <T>
 */
public interface DataAccessInterface <T> {
	public List<T> findAll();
	public boolean create(T t);
	public boolean update(T t);
	public boolean delete(T t);

}
