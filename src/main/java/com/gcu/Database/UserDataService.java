package com.gcu.Database;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcu.DataAccessInterface;
import com.gcu.UserDataAccessInterface;
import com.gcu.UserEntity;
import com.gcu.UserRepository;
import com.gcu.Models.UserModel;

@Service
public class UserDataService implements UserDataAccessInterface<UserModel>, DataAccessInterface<UserModel> {

	@Autowired
	private UserRepository userRepository;

	public UserModel findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	public UserDataService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public List<UserModel> findAll() {
		// TODO Auto-generated method stub
		try {
		List<UserModel> allUsers = new ArrayList<UserModel>();
		userRepository.findAll().forEach(allUsers::add);
		System.out.println("Found all" + allUsers.toString());
		return allUsers;
		} catch (Exception e) {
			return new ArrayList<UserModel>();
		}
	}

	public UserModel findById(String id) {
		try {
			return userRepository.findById(id).orElse(null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean create(UserModel t) {
		System.out.println(t.toString() + " created!");
		try {
			
			System.out.println("created");
			userRepository.save(t);
		return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(UserModel t) {
		try {
			userRepository.deleteById(t.getId());
			userRepository.save(t);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteById(String i) {
		try {
			System.out.println("deleted" + i);
			userRepository.deleteById(i);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(UserModel t) {
		// TODO Auto-generated method stub
		return false;
	}

}
