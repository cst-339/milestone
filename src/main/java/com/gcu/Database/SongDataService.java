package com.gcu.Database;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import com.gcu.UserEntity;
import com.gcu.Models.SongModel;
import com.gcu.Models.UserModel;

/**
 * This interface is in charge of accessing songs through database calls and connecting to external private REST API
 * @author joshbeck
 *
 */
@Service
public class SongDataService implements DataAccessInterface<SongModel> {

	/**
	 * Autowired instance of the song repository to access the database
	 */
	@Autowired
	private SongRepository songRepository;


	/**
	 * Initializer to allow overwriting the autowired song repository
	 * @param songRepository
	 */
	public SongDataService(SongRepository songRepository) {
		this.songRepository = songRepository;

		
	}
	
	/**
	 * Method for finding all songs.  Connects to external REST API that reads JSON from a database and returns a list of SongModels
	 */
	@Override
	public List<SongModel> findAll() {
		// TODO Auto-generated method stub
		try {
			System.out.println("Getting Songs");
			String hostname = "localhost";
			int port = 8091;
			String url = "http://" + hostname + ":" + port + "/service/songs";
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<List<SongModel>> rateResponse = restTemplate.exchange(url,  HttpMethod.GET, null, new ParameterizedTypeReference<List<SongModel>>() {});
			List<SongModel> songs = rateResponse.getBody();
			return songs;
		} catch (Exception e) {
			return new ArrayList<SongModel>();
		}
	}

	/**
	 * Connects to external REST API and finds a song model by redone title if one is found.  Otherwise, an invalid response body is returned
	 * @param redoneTitle
	 * @return
	 */
	public SongModel findByTitle(String redoneTitle) {
		try {
			String hostname = "localhost";
			int port = 8091;
			String url = "http://" + hostname + ":" + port + "/service/withtitle?title=" + redoneTitle;
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<SongModel> rateResponse = restTemplate.exchange(url,  HttpMethod.GET, null, new ParameterizedTypeReference<SongModel>() {});
			SongModel song = rateResponse.getBody();
			return song;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Connects directly to the database and creates a new song model in database by using the song repository .save() method
	 */
	@Override
	public boolean create(SongModel t) {
		try {
			songRepository.save(t);
		return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Connects directly to database and overwrites the song model already in database
	 */
	@Override
	public boolean update(SongModel t) {
		try {
			songRepository.save(t);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Delete song by id
	 * @param i - Id
	 * @return - was deleted or not
	 */
	public boolean deleteById(String i) {
		try {
		
			songRepository.deleteById(i);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Connects directly to the database and deletes the song model in the database if the song exists
	 */
	@Override
	public boolean delete(SongModel t) {
		try {
			songRepository.delete(t);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	

}
