package com.gcu;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

/**
 * Interface for handling user database requests
 * @author joshbeck
 *
 * @param <T>
 */
public interface UserDataAccessInterface <T> {
	public T findByUsername(String username);
}
