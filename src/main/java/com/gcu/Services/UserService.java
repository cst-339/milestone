package com.gcu.Services;

import java.security.SecureRandom;

import javax.naming.spi.ObjectFactory;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gcu.Database.UserDataService;
import com.gcu.Models.UserModel;

/**
 * This service acts as a communicator between the data access classes and the controller classes.
 * Specifically, this service is focused on communication regarding the user profile between the database and the Login/RegisterControllers.
 * 
 * @author joshbeck
 *
 */
@Service
public class UserService {
	
	@Autowired
	UserDataService service;

	/**
	 * This method handles validating registration of a user and ensuring a complete DB
	 * save.  As of Milestone 2, it emulates interacting with a database by ensuring 
	 * the registration user name is NOT joseph_smith.  If it is, it will return a failure
	 * @param user
	 * @return returns true if no conflicting copies in DB
	 */
	public boolean register(UserModel user) {

		 BCryptPasswordEncoder bCryptPasswordEncoder =
		  new BCryptPasswordEncoder();
		 String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
		 user.setPassword(encodedPassword);
		 
		return service.create(user);
	}
	
	/**
	 * This method handles interacting with the session to remove the "loggedIn" token
	 * connoting that a user has logged in within the session.
	 */
	public void logout() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		session.setAttribute("loggedIn", false);
	}
	
	/**
	 * This method returns whether the session variable indicates the user is logged in
	 * @return - Whether the user has logged in within the current session
	 */
	public boolean isUserLoggedIn() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		return session.getAttribute("loggedIn") == null ? false : (boolean)session.getAttribute("loggedIn");
	}
}
