package com.gcu.Models;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

/**
 * This class is a model class that holds all information pertaining to a user.
 * In addition to providing data structure, it also provides an outline of the appropriate
 * data validation for the Spring framework to check
 * @author joshbeck
 *
 */
@Document(collection="usersmilestone")
public class UserModel {

	//Properties
	@Id
	private String id;
	
	private String firstName;

	private String lastName;

	private String email;

	private String phoneNum;

	private String username;
	
	private String password;
	
	//Getter/setters
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	//Constructors
	public UserModel(String id, String firstName, String lastName, String email, String phoneNum, String username,
			String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNum = phoneNum;
		this.username = username;
		this.password = password;
	}
	public UserModel() {
		super();
	}
	
	
}
