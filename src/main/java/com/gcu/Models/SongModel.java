package com.gcu.Models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

/**
 * This model holds the song information for a new or existing redone song listing entry
 * @author joshbeck
 *
 */

@Document(collection="songmilestone")
public class SongModel {

	public SongModel() {
		super();
	}

	public SongModel(@NotNull String id,
			@NotNull(message = "You must include a title for your redone song") @Length(min = 1, max = 20, message = "The redone song title must be between 1 and 20 characters long") String redoneTitle,
			@Length(min = 1, max = 20, message = "The redone song subtitle must be between 1 and 20 characters long") String redoneSubtitle,
			@NotNull(message = "You must include a link to your redone song (YouTube, Vimeo, etc.)") String redoneSongLink,
			@NotNull(message = "You must include a title for the original song you have remade") @Length(min = 1, max = 100, message = "The original song title must be between 1 and 100 characters long") String originalTitle,
			@NotNull(message = "You must include the artist that made the original song for copyright reasons") @Length(min = 1, max = 100, message = "The artist's name must be between 1 and 100 characters long") String originalArtist,
			@NotNull(message = "You must include a link to your redone song (YouTube, Vimeo, etc.)") String originalSongLink,
			@Length(min = 1, max = 1000, message = "The original song title must be between 1 and 1000 characters long") String songDescription,
			String redoneAlbumCoverLink) {
		super();
		this.id = id;
		this.redoneTitle = redoneTitle;
		this.redoneSubtitle = redoneSubtitle;
		this.redoneSongLink = redoneSongLink;
		this.originalTitle = originalTitle;
		this.originalArtist = originalArtist;
		this.originalSongLink = originalSongLink;
		this.songDescription = songDescription;
		this.redoneAlbumCoverLink = redoneAlbumCoverLink;
	}

	//Properties
	@Id
	private String id;
	

	private String redoneTitle;

	private String redoneSubtitle;
	
	private String redoneSongLink;

	private String originalTitle;

	private String originalArtist;
	
	private String originalSongLink;
	
	private String songDescription;
	
	private String redoneAlbumCoverLink;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRedoneTitle() {
		return redoneTitle;
	}

	public void setRedoneTitle(String redoneTitle) {
		this.redoneTitle = redoneTitle;
	}

	public String getRedoneSubtitle() {
		return redoneSubtitle;
	}

	public void setRedoneSubtitle(String redoneSubtitle) {
		this.redoneSubtitle = redoneSubtitle;
	}

	public String getRedoneSongLink() {
		return redoneSongLink;
	}

	public void setRedoneSongLink(String redoneSongLink) {
		this.redoneSongLink = redoneSongLink;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getOriginalArtist() {
		return originalArtist;
	}

	public void setOriginalArtist(String originalArtist) {
		this.originalArtist = originalArtist;
	}

	public String getOriginalSongLink() {
		return originalSongLink;
	}

	public void setOriginalSongLink(String originalSongLink) {
		this.originalSongLink = originalSongLink;
	}

	public String getSongDescription() {
		return songDescription;
	}

	public void setSongDescription(String songDescription) {
		this.songDescription = songDescription;
	}

	public String getRedoneAlbumCoverLink() {
		return redoneAlbumCoverLink;
	}

	public void setRedoneAlbumCoverLink(String redoneAlbumCoverLink) {
		this.redoneAlbumCoverLink = redoneAlbumCoverLink;
	}
	
}