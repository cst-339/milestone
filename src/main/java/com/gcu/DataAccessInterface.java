package com.gcu;

import java.util.List;

/**
 * This interface provides a framework for accessing data
 * @author joshbeck
 *
 * @param <T> - The type of data being accessed
 */
public interface DataAccessInterface <T> {
	public List<T> findAll();
	public T findById(String id);
	public boolean create(T t);
	public boolean update(T t);
	public boolean delete(T t);
}
